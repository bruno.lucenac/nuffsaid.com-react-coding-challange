import faker from 'faker';
import random from 'lodash/random';
import {
  createContext,
  useContext,
  useEffect,
  useRef,
  useState
} from 'react';
import { Message, Priority } from './components/models';

type MessagesProviderProps = { children: React.ReactNode }
type MessagesValue = {
  clearMessage: (messageId: number) => void;
  clearMessages: () => void;
  getMessagesByPriority: (priority: Priority) => Message[];
  isStarted: boolean;
  messages: Message[];
  start: () => void;
  stop: () => void;
}

const MessagesContext = createContext<MessagesValue | undefined>(undefined);

/**
 * This provider will create new messages on a random setTimeout time, giving
 * ability to start/stop the messages, clear individually and clear all of them.
 */
function MessagesProvider({ children }: MessagesProviderProps) {
  const [nextId, setNextId] = useState(1);
  const [messages, setMessages] = useState<Message[]>([]);
  const [stopGeneration, setStopGeneration] = useState(false);

  // We need to use refs so that the values inside setTimeout are taken from
  // the moment thet the code is executed. Otherwise, the values will be taken
  // from the moment setTimeout was called, causing errors.
  const stopGenerationRef = useRef(stopGeneration);
  stopGenerationRef.current = stopGeneration;
  const nextIdRef = useRef(nextId);
  nextIdRef.current = nextId;

  /**
   * Clear a specific message
   */
  const clearMessage = (messageId: number) => {
    setMessages(currentMessages => currentMessages.filter(message => message.id !== messageId));
  }

  /**
   * Clear all messages
   */
  const clearMessages = () => {
    setMessages([]);
  }

  /**
   * priority from 1 to 3, 1 = Error, 2 = Warning, 3 = Info
   */
  const generate = () => {
    if (stopGenerationRef.current) {
      return;
    }

    const message = faker.lorem.sentence();
    const priority = random(1, 3);

    // Callback is needed so that we always use the updated value
    setMessages((currentMessages) => {
      return [
        {
          id: nextIdRef.current,
          message,
          priority,
        },
        ...currentMessages,
      ]
    });
    setNextId(nextIdRef.current + 1);

    const nextInMS = random(500, 3000)

    setTimeout(() => {
      generate()
    }, nextInMS)
  }

  /**
   * Return all messages with a specific priority
   */
  const getMessagesByPriority = (priority: Priority) => {
    return messages.filter(message => message.priority === priority);
  }

  const start = () => {
    setStopGeneration(false);
  }

  const stop = () => {
    setStopGeneration(true);
  }

  // This will trigger generate correctly with start/stop functions
  useEffect(() => {
    if (!stopGeneration) {
      generate();
    }
  }, [stopGeneration]);

  const isStarted = !stopGeneration;

  return (
    <MessagesContext.Provider
      value={{
        clearMessage,
        clearMessages,
        getMessagesByPriority,
        isStarted,
        messages,
        start,
        stop,
      }}
    >
      {children}
    </MessagesContext.Provider>
  );
}

function useMessages() {
  const context = useContext(MessagesContext);

  // This check is to satisfy typescript
  if (context === undefined) {
    throw new Error('useMessages must be used within a MessagesProvider')
  }

  return context;
}

export { MessagesProvider, useMessages };