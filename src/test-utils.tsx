import { FC, PropsWithChildren, ReactElement } from 'react';
import { MessagesProvider } from './useMessages';
import { render, RenderOptions } from '@testing-library/react';

const AllTheProviders: FC = ({ children }: PropsWithChildren<any>) => {
  return (
    <MessagesProvider>
      {children}
    </MessagesProvider>
  );
};

const customRender = (
  ui: ReactElement,
  options?: Omit<RenderOptions, 'queries'>,
) => render(ui, { wrapper: AllTheProviders, ...options });

export * from '@testing-library/react';

export { customRender as render };
