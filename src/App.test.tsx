import App from './App';
import userEvent from '@testing-library/user-event';
import {
  render,
  screen,
  waitFor,
  within
} from './test-utils';
import '@testing-library/jest-dom';

test('should have buttons start/stop and clear', () => {
  render(<App />);

  expect(screen.getByRole('button', { name: /start stop button/i })).toBeInTheDocument();
  expect(screen.getByRole('button', { name: /clear messages button/i })).toBeInTheDocument();
});

test('should have messages API started on first load', () => {
  render(<App />);

  expect(screen.getByText(/stop messages/i)).toBeInTheDocument();
});

test('should have 3 lists with count: Error, Warning and Info', () => {
  render(<App />);

  const errorListWrapper = screen.getByTestId(/list error wrapper/i);
  const warningListWrapper = screen.getByTestId(/list warning wrapper/i);
  const infoListWrapper = screen.getByTestId(/list info wrapper/i);

  expect(within(errorListWrapper).getByRole('heading', { name: /error/i }));
  expect(within(warningListWrapper).getByRole('heading', { name: /warning/i }));
  expect(within(infoListWrapper).getByRole('heading', { name: /info/i }));

  expect(within(errorListWrapper).getByRole('heading', { name: /count/i }));
  expect(within(warningListWrapper).getByRole('heading', { name: /count/i }));
  expect(within(infoListWrapper).getByRole('heading', { name: /count/i }));

  expect(within(errorListWrapper).getByRole('list', { name: /list error/i }));
  expect(within(warningListWrapper).getByRole('list', { name: /list warning/i }));
  expect(within(infoListWrapper).getByRole('list', { name: /list info/i }));
});

test('should stop/start messages when clicking on button', () => {
  render(<App />);

  const button = screen.getByRole('button', { name: /start stop button/i });

  // The initial text on the button should be "Stop Messages"
  expect(button).toBeInTheDocument();
  expect(screen.getByText(/stop messages/i)).toBeInTheDocument();

  // Click the button
  userEvent.click(button);

  // The text of the button should change
  expect(screen.getByText(/start messages/i)).toBeInTheDocument();

  // Click the button again
  userEvent.click(button);

  // Text should change again
  expect(screen.getByText(/stop messages/i)).toBeInTheDocument();
});

// Increase jest timeout since we'll wait a few seconds on the next tests
jest.setTimeout(10000);

test('should have new messages after a few seconds', async () => {
  render(<App />);

  // Arbitrary wait for 3 seconds so that some messages can be populated after rendering the App
  await new Promise((r) => setTimeout(r, 3000));

  // Stop the messages
  userEvent.click(screen.getByRole('button', { name: /start stop button/i }));

  const errorListWrapper = screen.getByTestId(/list error wrapper/i);
  const warningListWrapper = screen.getByTestId(/list warning wrapper/i);
  const infoListWrapper = screen.getByTestId(/list info wrapper/i);

  // Get the counts rendered on the header of each list
  const errorCount = parseInt(within(errorListWrapper).getByTestId('count').innerHTML);
  const warningCount = parseInt(within(warningListWrapper).getByTestId('count').innerHTML);
  const infoCount = parseInt(within(infoListWrapper).getByTestId('count').innerHTML);

  // Get the list of messages in each column. We need to use query instead of get because some columns may be empty
  const errorMessages = within(errorListWrapper).queryAllByRole('listitem');
  const warningMessages = within(warningListWrapper).queryAllByRole('listitem');
  const infoMessages = within(infoListWrapper).queryAllByRole('listitem');

  // Any of the counts should be greater than 0.
  // Since they are created randomly, we can't know how many each column will have exactly,
  // so we just check that any messages were added and that the counts are correct
  expect(errorCount + warningCount + infoCount).toBeGreaterThan(0);

  expect(errorCount).toBe(errorMessages.length);
  expect(warningCount).toBe(warningMessages.length);
  expect(infoCount).toBe(infoMessages.length);
});

test('should clear an individual message', async () => {
  render(<App />);

  // Arbitrary wait for 3 seconds so that some messages can be populated after rendering the App
  await new Promise((r) => setTimeout(r, 3000));

  // Stop the messages
  userEvent.click(screen.getByRole('button', { name: /start stop button/i }));

  // Since the messages are created with sequential ids, we get the message with id 1,
  // which should be expected to be on the screen after waiting for 3 seconds
  const firstMessageCreated = screen.getByRole('listitem', { name: /message 1/i });

  expect(firstMessageCreated).toBeInTheDocument();

  // Click on clear button inside that message
  userEvent.click(within(firstMessageCreated).getByRole('button', { name: /clear message/i }));

  // Check that the message is no longer in the screen
  expect(firstMessageCreated).not.toBeInTheDocument();
});

test('should clear all messages', async () => {
  render(<App />);

  // Arbitrary wait for 3 seconds so that some messages can be populated after rendering the App
  await new Promise((r) => setTimeout(r, 3000));

  // Stop the messages
  userEvent.click(screen.getByRole('button', { name: /start stop button/i }));

  // Check that there are messages on the screen
  expect(screen.getAllByRole('listitem').length).toBeGreaterThan(0);

  // Click on clear button
  userEvent.click(screen.getByRole('button', { name: /clear messages button/i }));

  // Check that there're no messages on the screen
  expect(screen.queryAllByRole('listitem').length).toBe(0);
});

// Increase jest timeout a lot, because we'll have to wait for at least one error message to be
// created to run the next test successfully.
// 1 minute should be more that enough to wait for at least one error message.
jest.setTimeout(60000);

test('should open Snackbar when receiving an error message and clear it after 2 seconds', async () => {
  render(<App />);

  // We we'll wait for an alert to show up on the screen
  waitFor(() => expect(screen.getByRole('alert')).toBeInTheDocument(), { timeout: 60000 });

  // After one alert shows up, we stop the messages
  userEvent.click(screen.getByRole('button', { name: /start stop button/i }));

  // We then wait for 2 seconds, which is the default timeout for the Snackbar to close
  await new Promise((r) => setTimeout(r, 2000));

  // And then check that the alert is no longer there
  expect(screen.queryByRole('alert')).not.toBeInTheDocument();
});
