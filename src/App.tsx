import Messages from './components/Messages';
import { MessagesProvider } from './useMessages';
import 'normalize.css';

export default function App() {
  return (
    <MessagesProvider>
      <Messages />
    </MessagesProvider>
  );
}
