import React from "react";
import { ContainerStyle } from "./styles";

export default function Container({ children }: React.PropsWithChildren<any>) {
  return (
    <ContainerStyle>
      {children}
    </ContainerStyle>
  )
}
