import styled from "styled-components";

export const Wrapper = styled.div`
  border-radius: 5px;
`;

export const Header = styled.header`
  > * {
    &:not(:first-child) {
      margin-top: 5px;
    }
  }
`;

export const Title = styled.h2`
  margin: 0;
  color: #000000;
  font-size: 24px;
  font-weight: 600;
`;

export const Count = styled.h3`
  margin: 0;
  color: #000000;
  font-size: 18px;
  font-weight: 400;
`;

export const List = styled.ul`
  display: grid;
  gap: 11px;
  margin-top: 20px;
  padding: 0;
`;
