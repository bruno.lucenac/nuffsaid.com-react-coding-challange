import Message from '../Message';
import { AnimatePresence, motion } from 'framer-motion';
import {
  Count,
  Header,
  List,
  Title,
  Wrapper
} from './styles';
import { Priority } from '../models';
import { useMessages } from '../../useMessages';

interface Props {
  priority: Priority;
}

/**
 * List of messages of a specific priority, with animations on the messages.
 */
export default function MessageList({ priority }: Props) {
  const { getMessagesByPriority } = useMessages();

  const getHeaderText = () => {
    switch (priority) {
      case Priority.Error:
        return 'Error';

      case Priority.Warning:
        return 'Warning';

      case Priority.Info:
        return 'Info';

      default:
        return '';
    }
  };

  const headerText = getHeaderText();
  const messages = getMessagesByPriority(priority);

  return (
    <Wrapper data-testid={`List ${headerText} Wrapper`}>
      <Header>
        <Title>
          {headerText}
        </Title>

        <Count>
          Count (<span data-testid="count">{messages.length}</span>)
        </Count>
      </Header>

      <AnimatePresence>
        <List aria-label={`List ${headerText}`}>
          {messages.map((message, index) => {
            return (
              <motion.div
                animate={{ y: 0 }}
                key={`${message.id} ${index}`}
                initial={{ y: 50 }}
                exit={{ y: 50 }}
              >
                <Message {...message} />
              </motion.div>
            );
          })}
        </List>
      </AnimatePresence>
    </Wrapper>
  )
}
