import styled from "styled-components";

export const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  gap: 22px;
  padding: 40px 10px;

  @media screen and (max-width: 600px) {
    grid-template-columns: 1fr;
  }
`;
