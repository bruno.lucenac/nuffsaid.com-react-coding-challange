import MessageList from '../MessageList';
import { Grid } from './styles';
import { Priority } from '../models';

/**
 * All lists aligned
 */
export default function MessageLists() {
  return (
    <Grid>
      <MessageList priority={Priority.Error} />
      <MessageList priority={Priority.Warning} />
      <MessageList priority={Priority.Info} />
    </Grid>
  )
}
