import Alert, { Color } from '@material-ui/lab/Alert';
import SnackbarMaterial from '@material-ui/core/Snackbar';
import { useEffect, useState } from 'react';

export const defaultInterval = 250;

export interface SnackbarProps {
  autoHideDuration?: number;
  isOpen: boolean;
  onClose?: () => any;
  text: string;
  type: Color;
}

/**
 * Wrapper over MaterialUI Snackbar and Alert
 * 
 * refs:
 *  https://material-ui.com/pt/components/snackbars/
 *  https://material-ui.com/pt/components/alert/
 */
export default function Snackbar({
  autoHideDuration = 2000,
  onClose,
  isOpen = false,
  text = '',
  type = 'error',
}: SnackbarProps) {
  const [open, setOpen] = useState(false);

  // Manages all of snackbar's opening process
  const openSnackbar = () => {
    // Closes the snackbar if it is already open and open again
    // This ensures that a new animation will happen when opening a snackar and clearing
    // an older one at the same time.
    if (open) {
      setOpen(false);

      setTimeout(() => {
        setOpen(true);
      }, defaultInterval);
    } else {
      setOpen(true);
    }
  };

  const closeSnackbar = () => {
    setOpen(false);

    // After closing the Snackbar internally, execute the callback passed to
    // the component if there's one.
    if (onClose) {
      onClose();
    }
  };

  // Since the component has an internal open state, this effect syncs this
  // internal state with the external one.
  useEffect(() => {
    if (isOpen) {
      openSnackbar();
    }
  }, [isOpen])

  return (
    <SnackbarMaterial
      anchorOrigin={{ horizontal: 'center', vertical: 'top' }}
      autoHideDuration={autoHideDuration}
      onClose={closeSnackbar}
      open={open}
    >
      <Alert onClose={closeSnackbar} severity={type}>
        {text}
      </Alert>
    </SnackbarMaterial>
  );
}