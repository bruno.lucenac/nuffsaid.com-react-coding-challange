export enum Priority {
  Error = 1,
  Warning = 2,
  Info = 3,
};

export interface Message {
  id: number;
  message: string;
  priority: Priority;
}