import Container from './Container';
import MessageHeader from './MessageHeader';
import MessageLists from './MessageLists';
import Snackbar from './Snackbar';
import { Message, Priority } from './models';
import { useEffect, useState } from 'react';
import { useMessages } from '../useMessages';

export default function Messages() {
  const [activeMessageOnSnackbar, setActiveMessageOnSnackbar] = useState<Message | undefined>(undefined);
  const [isSnackbarOpen, setIsSnackbarOpen] = useState(false);
  const { getMessagesByPriority } = useMessages();

  const errorMessages = getMessagesByPriority(Priority.Error);

  const onCloseSnackbar = () => {
    setIsSnackbarOpen(false);
  }

  // Open snackbar if a new error message is received
  useEffect(() => {
    const incomingMessage = errorMessages[0];

    if (incomingMessage && incomingMessage.id !== activeMessageOnSnackbar?.id) {
      setActiveMessageOnSnackbar(incomingMessage);
      setIsSnackbarOpen(true);
    }
  }, [errorMessages])

  return (
    <>
      <Container>
        <MessageHeader />
        <MessageLists />
      </Container>

      <Snackbar
        isOpen={isSnackbarOpen}
        onClose={onCloseSnackbar}
        text={activeMessageOnSnackbar?.message ?? ''}
        type="error"
      />
    </>
  )
}
