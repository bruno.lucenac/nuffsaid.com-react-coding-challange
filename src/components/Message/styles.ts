import styled from "styled-components";

export const Wrapper = styled.li<{ backgroundColor: string }>`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: 110px;
  padding: 12px 14px;
  border-radius: 5px;
  background-color: ${(props) => props.backgroundColor};
  box-shadow: 0px 2px 6px 1px #c5c5c5;
  list-style-type: none;
`;

export const Title = styled.h4`
  margin: 0;
  font-size: 16px;
`;

export const Actions = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 10px;
`;
