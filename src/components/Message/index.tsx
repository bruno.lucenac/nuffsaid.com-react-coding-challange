import Button from '@material-ui/core/Button';
import { Actions, Title, Wrapper } from './styles';
import { Message as IMessage, Priority } from '../models';
import { useMessages } from '../../useMessages';

interface Props extends IMessage { }

/**
 * Card with individual message
 */
export default function Message({
  id,
  message,
  priority,
}: Props) {
  const { clearMessage } = useMessages();

  const getColor = () => {
    switch (priority) {
      case Priority.Error:
        return "#f66136";

      case Priority.Warning:
        return "#fbe788";

      case Priority.Info:
        return "#88fca3";

      default:
        return "#88fca3";
    }
  }

  return (
    <Wrapper aria-label={`message ${id}`} backgroundColor={getColor()}>
      <Title>
        {message}
      </Title>

      <Actions>
        <Button
          aria-label="clear message"
          name="clear message"
          onClick={() => clearMessage(id)}
          style={{ fontWeight: 600 }}
          variant="text"
        >
          Clear
        </Button>
      </Actions>
    </Wrapper>
  );
}