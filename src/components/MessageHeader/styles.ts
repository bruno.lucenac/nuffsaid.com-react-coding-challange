import styled from "styled-components";

export const Content = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 20px;

  > * {
    &:first-child {
      margin-right: 10px;
    }
  }
`;
