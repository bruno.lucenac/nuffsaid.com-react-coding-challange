import Button from '@material-ui/core/Button';
import { Content } from './styles';
import { useMessages } from '../../useMessages';

/**
 * Header with buttons Start/Stop and Clear
 */
export default function MessageHeader() {
  const { clearMessages, isStarted: isApiStarted, start, stop } = useMessages();

  return (
    <Content>
      <Button
        aria-label="start stop button"
        color={isApiStarted ? "secondary" : "primary"}
        name="start stop button"
        onClick={() => {
          if (isApiStarted) {
            stop();
          } else {
            start();
          }
        }}
        variant="contained"
      >
        {isApiStarted ? 'Stop Messages' : 'Start Messages'}
      </Button>

      <Button
        aria-label="clear messages button"
        name="clear messages button"
        onClick={clearMessages}
        variant="contained"
      >
        Clear
      </Button>
    </Content>
  );
}
